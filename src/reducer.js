import { LEN } from './constants';

const aliveOrDead = () => Math.floor(Math.random() * 2);

const flatten = (arr) => [].concat(...arr).join('');

const equal = (arr1, arr2) => (arr1 === arr2);

const init = () => {
    let lives = (new Array(LEN)).fill(null);
    lives = lives.map(() => (new Array(LEN)).fill(null).map(() => aliveOrDead()));

    const history = [flatten(lives)];

    return {
        lives: lives,
        history: history,
        isRunning: true,
    }
}


const update = (state) => {
    let lives = new Array(LEN);

    const neighborInd = [
        [-1, -1], [-1, 0], [-1, 1],
        [0, -1], [0, 1],
        [1, -1], [1, 0], [1, 1]
    ]

    for (let i = 0; i < LEN; i++) {
        lives[i] = new Array(LEN);
        for (let j = 0; j < LEN; j++) {
            let neighbor = 0;
            for (let offset of neighborInd) {
                const x = i + offset[0];
                const y = j + offset[1];
                if (x >= 0 && x < LEN && y >= 0 && y < LEN) {
                    neighbor += state.lives[x][y];
                }
            }

            if (neighbor == 3) {
                lives[i][j] = 1;
            } else if (neighbor == 2) {
                lives[i][j] = state.lives[i][j];
            } else {
                lives[i][j] = 0;
            }
        }
    }

    const history = [flatten(state.lives), state.history[0]];
    const flatLives = flatten(lives);
    let isRunning = state.isRunning;
    if (equal(history[0], flatLives) || equal(history[1], flatLives)) {
        isRunning = false;
    }

    return {
        ...state,
        lives: lives,
        history: history,
        isRunning: isRunning,
    };
}


const setAlive = (state, x, y) => {
    let lives = state.lives.map((line, i) => line.map((item, j) => ((i == x || j == y) ? 1 : item)));
    return {
        ...state,
        lives: lives
    };
}


export default (state = init(), action) => {
    switch (action.type) {
        case "cell":
            return setAlive(state, action.x, action.y);
        case "tick":
            return update(state);
        case "restart":
            return init();
        default:
            return state;
    }
}
