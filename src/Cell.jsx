import React from 'react';

import { connect } from 'react-redux';

import * as actions from './actions';

class Cell extends React.Component {
    handleClickCell(x, y) {
        const { clickCell } = this.props;
        clickCell(x, y);
    }
    render() {
        const { isAlive, x, y } = this.props;
        const liveStatus = isAlive ? "live" : "dead";
        return (
            <div className={'cell ' + liveStatus} onClick={() => this.handleClickCell(x, y)}></div>
        )
    }
}
export default connect((state) => ({ state: state }), actions)(Cell);

