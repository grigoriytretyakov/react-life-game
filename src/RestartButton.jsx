import React from 'react';

import { connect } from 'react-redux';

import * as actions from './actions';

class RestartButton extends React.Component {
    clickRestart() {
        const { restart } = this.props;
        restart();
    }

    render() {
        return (
            <button onClick={() => this.clickRestart()}>Restart</button>
        )
    }
}
export default connect(() => ({}), actions)(RestartButton);