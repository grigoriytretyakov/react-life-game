import React from 'react';

import { LEN } from './constants';

import Line from './Line.jsx';

export default class Table extends React.Component {
    render() {
        let rows = []
        for (let i = 0; i < LEN; i++) {
            rows.push(<Line key={'line-' + i} line={i} />)
        }

        return (
            <div>
                {rows}
            </div>
        )
    }
}
