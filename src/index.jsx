import React from 'react';
import ReactDOM from 'react-dom';

import { createStore } from 'redux';
import { Provider } from 'react-redux';

import reducer from './reducer';
import Life from './Life.jsx';

const store = createStore(reducer);

ReactDOM.render(
    <Provider store={store}>
        <Life />
    </Provider>,
    document.getElementById('root')
)
