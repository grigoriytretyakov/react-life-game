import React from 'react';

import { connect } from 'react-redux';
import { LEN } from './constants';

import * as actions from './actions';
import Cell from './Cell.jsx';

class Line extends React.Component {
    render() {
        const { state, line } = this.props;

        let row = []
        for (let i = 0; i < LEN; i++) {
            row.push(<Cell isAlive={state.lives[line][i] == 1} key={i} x={line} y={i} />);
        }

        return (
            <div className="line">
                {row}
            </div>
        )
    }
}
export default connect((state) => ({ state: state }), actions)(Line);