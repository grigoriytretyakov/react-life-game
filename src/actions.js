
export const tick = () => ({
    type: 'tick'
})

export const restart = () => ({
    type: 'restart'
})

export const clickCell = (x, y) => ({
    type: 'cell',
    x,
    y
})

