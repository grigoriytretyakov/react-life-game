import React from 'react';
import { connect } from 'react-redux';
import TimerMixin from 'react-timer-mixin';

import * as actions from './actions';

import Table from './Table.jsx';
import RestartButton from './RestartButton.jsx';
import Modal from './Modal.jsx';

const createReactClass = require('create-react-class');

let Life = createReactClass({
    mixins: [TimerMixin],
    componentDidMount: function () {
        const { tick } = this.props;
        const fire = () => {
            this.setTimeout(
                () => {
                    tick();
                    fire();
                }, 
                50
            )
        }
        fire();
    },
    render: function () {
        const { state } = this.props;
        const modal = state.isRunning ? null : <Modal />;
        return (
            <div>
                <div className="header">
                    <span>Life</span>Game
                    <RestartButton />
                </div>
                <Table />
                {modal}
                <p>
                    <a href="https://gitlab.com/grigoriytretyakov/react-life-game">Source code</a>
                </p>
            </div>
        );
    }
});

export default connect((state) => ({ state: state }), actions)(Life);
